﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        public static double a, b, c = 0.00;


        static void Main(string[] args)
        {
            String aParam, bParam, cParam;
            Console.WriteLine("Podaj parametr a: ");
            aParam = Console.ReadLine();
            Console.WriteLine("Podaj parametr b: ");
            bParam = Console.ReadLine();
            Console.WriteLine("Podaj parametr c: ");
            cParam = Console.ReadLine();
            Validate(aParam, bParam, cParam);
            calculate();
            Console.ReadLine();



        }


        public static void Validate(String sA, String sB, String sC)
        {
            
            try
            {

                double.TryParse(sA, out a);
                double.TryParse(sB, out b);
                double.TryParse(sC, out c);

            }
            catch
            {
                Console.WriteLine("Podano nieprawidłowy format liczby");
            }


            if( a-b == 0)
            {
                Console.WriteLine("Próbowano wykonać dzielenie przez 0 ");
                Console.ReadLine();
            }
        }

        private static void calculate()
        {
            double result = 0.00;
            if( c > 0)
            {
                result = Math.Pow(a, 2) + b;
            }
            else if(c <0){
                result = a - Math.Pow(b, 2);
            }
            else if( c == 0)
            {
                result = 1 /(a - b);
            }


            Console.WriteLine("Wynik działania to : " + result);
        }
    }
}
