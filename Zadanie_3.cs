﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0, b = 0, aStrart = a;
            bool check = false;

            do
            {
                try
                {
                    Console.WriteLine("Podaj parametr a");
                    a = int.Parse(Console.ReadLine());
                    Console.WriteLine("Podaj parametr b");
                    b = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

            aStrart = a;
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }

            }
            Console.WriteLine("Największy wspólny dzielnik (NWD) liczby: " + aStrart + " to: " + a);
            Console.ReadLine();



        }
    }
}
