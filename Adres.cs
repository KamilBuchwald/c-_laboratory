using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    class Adres
    {

        private int houseNr, floatNr;
        private String cityName, postCode;
        public List<string> Adreses = new List<string>();


        public String PostCode
        {
            get
            {
                return postCode;
            }
            set
            {
                postCode = value;
            }
        }

        public int HouseNr
        {
            get
            {
                return houseNr;
            }
            set
            {
                houseNr = value;
            }
        }

        public int FloatNr
        {
            get
            {
                return FloatNr;
            }
            set
            {
                floatNr = value;
            }
        }

        public String CityName
        {
            get
            {
                return cityName;
            }
            set
            {
                cityName = value;
            }
        }
        public void saveAdres() {
            

            Adreses.Add(cityName);
            Adreses.Add(postCode);
            Adreses.Add(houseNr.ToString());
            Adreses.Add(floatNr.ToString());

           
        } 

        public void showAdreses()
        {
            var ileObiektow = Adreses.Count;
            Console.WriteLine(ileObiektow);

            //otrzymujemy liczbę 3
            string nowyTekst = string.Empty;

            foreach (string tekst in Adreses)
            {
                nowyTekst += " " + tekst;
            }
            
            Console.WriteLine(nowyTekst);
            Console.ReadLine();

        }
        


    }
    }
