using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    class Osoba
    {

        private String name;
        private String surname;
        private int birthYear;
        private int currentAge;
        private Adres adres;
        private int postCode, houseNr, floatNr;
        private String cityName;

        public Osoba()
        {
            name = "nieznany żołnierz";
            surname = "nieznany zołnierz";

        }

        public String Name
        {

            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public String Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }


        public int BirthdayYear
        {
            set
            {
                birthYear = value;
                currentAge = 2018 - birthYear;
            }
        }


        public String getData()
        {
            String Data;
            Data = name + " " + surname;
            return Data;
        }



        public int getBirthDate()
        {


            return birthYear;
        }

        public Adres Adres
        {
            get
            {
                if (this.adres == null)
                    this.adres = new Adres();
                return this.adres;
            }
        }
    }
    }
