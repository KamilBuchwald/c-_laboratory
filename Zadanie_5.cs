﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_5
{
    class Program
    {
        static void Main(string[] args)
        {
            firGenerator(Input());
        }



        private static int Input()
        {

            String Sa = " ";
            int a = 0;
            bool check = false;
            do
            {


                Console.WriteLine("Podaj Liczbę: ");
                try
                {
                    Sa = Console.ReadLine();
                    a = int.Parse(Sa);
                    check = true;
                    if (a <= 2)
                    {
                        Console.WriteLine("Liczba 1 i 2 nie sa liczbami pierwszymi");
                        check = false;
                    }
                    
                }
                catch
                {
                    Console.WriteLine("Podano błędny typ danych (tylko liczba całkowita");
                    check = false;
                }
            } while (check == false);
            return a;
        }


        private static void firGenerator(int a)
        {
            bool isFir = false;
            


            List<uint> pierwsze = new List<uint>();

            for (uint i = 2; i < a+2;  i++)
            {
                bool pierwsza = true;
                for (uint n = 2; n < i; n++)
                {
                    if (i % n == 0)
                    {
                        pierwsza = false;
                        break;
                    }
                }
                if (pierwsza && i == a)
                {
                    Console.WriteLine("Liczba " + a + " jest liczbą pierwszą" );
                    Console.ReadLine();
                    break;
                }
                if(i > a)
                {
                    Console.WriteLine("Liczba " + a + " nie jest liczbą pierwszą");
                    Console.ReadLine();
                }
              


            }
            
            

        }
    }
}
