using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    class Menu
    {

        public Menu()
        {
            this.buildMenu();
        }


        private void buildMenu()
        {
            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("                        ");
            Console.WriteLine("1.Dodaj adres           ");
            Console.WriteLine("2.Wyświetl adres        ");
            Console.WriteLine(" 1.Wszystkie            ");
            Console.WriteLine(" 2.Wyświetl według:     ");
            Console.WriteLine("     1 - Kod pocztowy   ");
            Console.WriteLine("     2 - Miasto         ");
            Console.WriteLine("     3 - Ulica          ");
            Console.WriteLine("     4 - Ulica, nr domu ");
            Console.WriteLine("     5 - Ulica, nr domu, nr mieszkania");
            Console.WriteLine("=========================================");
        

            int choice = 0;

            bool check = false;

            do
            {
                try
                {
                    Console.WriteLine("Wybierz: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

            switch (choice)
            {
                case 1:
                    Adres adres = new Adres();
                    adres.CityName = "Poznan";
                    adres.PostCode = "61-552";
                    adres.HouseNr = 10;
                    adres.FloatNr = 5;
                    adres.saveAdres();
                    break;

                case 2:
                    Console.WriteLine("Wyświetl adresy: ");
                    Console.WriteLine(" 1.Wszystkie            ");
                    Console.WriteLine(" 2.Wyświetl według:     ");
                    choice = 0;
                    check = false;
                    do
                    {
                        try
                        {
                            Console.WriteLine("Wybierz: ");
                            choice = int.Parse(Console.ReadLine());
                            check = true;
                        }
                        catch
                        {
                            check = false;
                            Console.WriteLine("Podano nieprawidłowy format danych");

                        }
                    } while (check == false);

                    switch (choice)
                    {
                        case 1:
                            Adres adres2 = new Adres();
                            adres2.showAdreses();
                          
                            break;
                        case 2:
                            break;


                    }
                    break;
            }







        }
    }
}
