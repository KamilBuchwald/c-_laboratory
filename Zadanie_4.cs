﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_4
{
    class Program
    {
        static void Main(string[] args)
        {

            Calculate(Input());

        }

        private static String Input()
        {

            String a = " ";
            bool check = false;
            do
            {


                Console.WriteLine("Podaj Liczbę: ");
                try
                {
                    a = Console.ReadLine();
                    int.Parse(a);
                    check = true;
                }
                catch
                {
                    Console.WriteLine("Podano błędny typ danych (tylko liczba całkowita");
                    check = false;
                }
            } while (check == false);
            return a;
        }


        private static void Calculate(String a)
        {
            Char[] number = new Char[a.Length];
            int result = 0;

            number = a.ToArray();
            for (int i = 0; i < number.Length; i++)
            {

                result = result + (int)Char.GetNumericValue(number[i]);
                
            }

            Console.WriteLine("Suma rozwinięcia dziesiętnego wynosi: " + result);
            Console.ReadLine();
        }

    }

}
